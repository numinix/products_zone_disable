<?php
//
// +----------------------------------------------------------------------+
// |zen-cart Open Source E-commerce                                       |
// +----------------------------------------------------------------------+
// | Copyright (c) 2007-2008 Numinix Technology http://www.numinix.com    |
// |                                                                      |
// | Portions Copyright (c) 2003-2006 Zen Cart Development Team           |
// | http://www.zen-cart.com/index.php                                    |
// |                                                                      |
// | Portions Copyright (c) 2003 osCommerce                               |
// +----------------------------------------------------------------------+
// | This source file is subject to version 2.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.zen-cart.com/license/2_0.txt.                             |
// | If you did not receive a copy of the zen-cart license and are unable |
// | to obtain it through the world-wide-web, please send a note to       |
// | license@zen-cart.com so we can mail you a copy immediately.          |
// +----------------------------------------------------------------------+
//  $Id: class.fec.php 85 2010-04-20 00:49:04Z numinix $
//
/**
 * Observer class used to redirect to the FEC page
 *
 */
class products_zones_disable extends base 
{
	function products_zones_disable() {
		global $zco_notifier;
		$zco_notifier->attach($this, array('NOTIFY_HEADER_START_CHECKOUT'));
	}
	
	function update(&$class, $eventID, $paramsArray) {
    global $db, $messageStack;
    $products = $_SESSION['cart']->get_products();
    $count = 0;
    foreach($products as $product) {
      $products_zones_disable = $db->Execute('SELECT products_zones_disable FROM ' . TABLE_PRODUCTS . ' WHERE products_id = ' . (int)$product['id']);
      $zones = $db->Execute('SELECT entry_zone_id FROM ' . TABLE_ADDRESS_BOOK . ' WHERE customers_id = ' . (int)$_SESSION['customer_id']);
      if(in_array($zones->fields['entry_zone_id'], explode(',', $products_zones_disable->fields['products_zones_disable'])) && (int)$_SESSION['customer_id'] != 0) {
        $count++;
        $messageStack->add_session('shopping_cart', sprintf('We can not ship the "%s" to your zone. Please remove this product and try again.', $product['name']));
      }
    }
    if($count > 0) {
      zen_redirect(zen_href_link('shopping_cart'));
    }
	}
}
// eof