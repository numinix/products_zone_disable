          <tr>
            <td colspan="2"><?php echo zen_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
          </tr>          
          <tr bgcolor="#DDEACC">
            <td class="main"><?php echo TEXT_PRODUCTS_ZONES_DISABLE; ?></td>
            <td class="main">
            <?php echo zen_draw_separator('pixel_trans.gif', '24', '15'); //. '&nbsp;' . zen_draw_input_field('products_zones_disable', $pInfo->products_zones_disable, zen_set_field_length(TABLE_PRODUCTS, 'products_zones_disable')); ?>
            <div style="overflow-y: scroll; height: 300px; width: 300px; border: 1px solid #ccc; padding: 10px; margin-bottom: 10px;">
            <?php 
            $products_zones_disable_current = explode(',', $pInfo->products_zones_disable);
            $zones = $db->Execute('SELECT * FROM ' . TABLE_ZONES . ' WHERE zone_country_id = 223 ORDER BY zone_name ASC');
            while(!$zones->EOF) {
              ?>
              <label style="display: block;"><?php echo zen_draw_checkbox_field('products_zones_disable[]', $zones->fields['zone_id'], in_array($zones->fields['zone_id'], $products_zones_disable_current) ? true : false)?>&nbsp;<?php echo $zones->fields['zone_name'];?></label>
              <?php 
              $zones->MoveNext();
            }
            ?>
            </div>
            </td>
          </tr>
          <tr>
            <td colspan="2"><?php echo zen_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
          </tr>